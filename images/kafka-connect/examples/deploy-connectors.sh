#!/bin/bash

set -eo pipefail

KAFKA_CONNECT_URL="http://kafka-cp-kafka-connect.midtier-dev.svc.cluster.local:8083"

update_connector_config() {
  local connector_name="$1"
  local connect_url="$2"
  local connector_file="$3"

  printf "\nAttempting to update config at %s/connectors/%s/config\n" "$connect_url" "$connector_name"
  result=$(curl -s -S -H 'Content-Type: application/json' \
    -H 'Accept: application/json' \
    -X PUT \
    "$connect_url"/connectors/"$connector_name"/config \
    --data-binary "@$connector_file")
  echo "$result"

  if [[ "$result" =~ .*"error_code".*  ]]; then
    echo -e "\nError returned.\n"
    exit 1
  fi
}

get_connector_config() {
  local connector_name="$1"
  local connect_url="$2"
  printf "\nAttempting to retrieve config at %s/connectors/%s/config\n" "$connect_url" "$connector_name"
  curl -s -S -i -H 'Content-Type: application/json' \
    -H 'Accept: application/json' \
    "$connect_url"/connectors/"$connector_name"/config
}

validate_connector_status() {
  local connector_name="$1"
  local connect_url="$2"
  printf "\nAttempting to retrieve connector status at %s/connectors/%s/status\n" "$connect_url" "$connector_name"
  result=$(curl -s -S -H 'Content-Type: application/json' \
    -H 'Accept: application/json' \
    "$connect_url"/connectors/"$connector_name"/status |
    jq -r .tasks[0].state)

    echo "Connector task state: $result"

    if [[ "$result" != "RUNNING"  ]]; then
      echo -e "\nError: connector task is not in RUNNING state.\n"
      exit 1
    fi
}

restart_connector_tasks() {
  local connector_name="$1"
  local connect_url="$2"
  printf "\nAttempting to retrieve tasks from %s/connectors/%s/tasks\n" "$connect_url" "$connector_name"
  tasks=$(curl -s -S -H 'Content-Type: application/json' \
    -H 'Accept: application/json' \
    "$connect_url"/connectors/"$connector_name"/tasks |
    jq .[].id.task)

  for task in $tasks; do
    printf "\nAttempting to restart task via %s/connectors/%s/tasks/%s/restart\n" "$connect_url" "$connector_name" "$task"
    curl -s -S -H 'Content-Type: application/json' \
      -H 'Accept: application/json' \
      -X POST \
      "$connect_url"/connectors/"$connector_name"/tasks/"$task"/restart
  done
}

cd "$(dirname "$0")"/connectors || exit

printf "Attempting to connect with kafka connect\n"
max_retry=12
counter=0

until curl --silent $KAFKA_CONNECT_URL/connectors
do
   sleep 8
   [[ counter -eq $max_retry ]] && echo "Failed!" && exit 1
   echo "Trying again. Try #$counter"
   ((counter++))
done

#to prevent expand in case there is no files in connectors dir
shopt -s nullglob

files=*.json

for f in $files; do

  connector_name=${f/.json}

  update_connector_config "$connector_name" "$KAFKA_CONNECT_URL" "$f"

  get_connector_config "$connector_name" "$KAFKA_CONNECT_URL"

  restart_connector_tasks "$connector_name" "$KAFKA_CONNECT_URL"

  echo "Wait 10 seconds before validating connector status"
  sleep 10

  validate_connector_status "$connector_name" "$KAFKA_CONNECT_URL"
done

# unset it now
shopt -u nullglob