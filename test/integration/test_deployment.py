from os import environ

import grainger.dp.testutil.k8s_objects as k8s_objects
import pytest
from grainger.dp.testutil import flaky_ext

CLUSTER = environ.get('CLUSTER')
ENV = environ.get('ENV')
K8S_VERSION = environ.get('K8S_VERSION')
NAMESPACE = environ.get('NAMESPACE')
MAX_RETRIES = 15
flaky_ext.WAIT_SECONDS = 30


class DeploymentConfig:
    def __init__(self, app_name, service_name):
        self.service_name = service_name
        self.app_name = app_name

    def __str__(self):
        return self.app_name


@pytest.mark.parametrize("deployment_config", [
    DeploymentConfig("cp-kafka-connect", "kafka-cp-kafka-connect") ], ids=DeploymentConfig.__str__)
class TestDeployment:
    @pytest.mark.flaky(max_runs=MAX_RETRIES, rerun_filter=flaky_ext.waiting_error_filter)
    def test_deployment_is_up_with_new_version(self, deployment_config):
        deployment = k8s_objects.Deployment(deployment_config.app_name, NAMESPACE)
        assert deployment.items()
        for c in deployment.ready_conditions():
            assert c.get('status') == 'True', 'pod "{}" {} status "{}"'.format(c.get('name'),
                                                                               c.get('type'),
                                                                               c.get('status'))
        for v in deployment.versions():
            assert v['version'] == K8S_VERSION, 'pod "{}" version (git SHA1)'.format(v['name'])